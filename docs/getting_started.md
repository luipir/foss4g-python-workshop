# Before We Start


## Help! I don't know Python!!?

All good. We will work though it as we go.  All examples will start of basic and add elements as we go along.

## Help! I don't know PyQGIS!?

That is also fine.  PyQGIS is just QGIS stuff on top of QGIS. We will work though it.

## Help! My coffee is cold.

That is something I can't do much about sorry.
